import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import{ getDatabase,onValue,ref,get,set,child,update,remove }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import{ getStorage, ref as refS,uploadBytes,getDownloadURL }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyC5EIXXtvPaYAE5W2-msBAEkk6db46N3YA",
    authDomain: "proyecto-70106.firebaseapp.com",
    databaseURL: "https://proyecto-70106-default-rtdb.firebaseio.com",
    projectId: "proyecto-70106",
    storageBucket: "proyecto-70106.appspot.com",
    messagingSenderId: "357413866360",
    appId: "1:357413866360:web:024fab8332c9693336168d",
    measurementId: "G-RTYBMGS4VZ"
};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db= getDatabase();

var IdProducto;
var NombreProducto;
var Descripcion;
var Cantidad;
var Estatus;
var Precio;
var Url;
var url;
var status;
var Formulario=document.querySelector('formulario');
var v;



function insertarDatos()
{
    leerInputs();
    set(ref(db,'productos/'+IdProducto),{
        NombreProducto:NombreProducto,Descripcion:Descripcion,Cantidad:Cantidad,Precio:Precio,Estatus:Estatus,Url:Url
    }).then((resp)=>{
        alert("Se Realizo el Registro");
        limpiarInputs();
    }).catch((error)=>{
        alert("ERROR"+" "+error);
    })
}

function actualizarDatos()
{
    leerInputs();

    if(IdProducto=="" || NombreProducto=="" || Descripcion=="" ||Cantidad==""||Estatus==""||Precio=="")
    {
        alert("NO DEJE CAMPOS EN VACIOS")
       
    }
    else if(Url=="")
    {
        alert("INSERTE UNA IMAGEN");
        
    }
    else
    {
        update(ref(db,'productos/'+IdProducto),{
            NombreProducto:NombreProducto,Descripcion:Descripcion,Cantidad:Cantidad,Precio:Precio,Estatus:Estatus,Url:Url
    
        }).then(resp=>{
            alert("Se realizo la Actualizacion con Exito.");
            limpiarInputs();
            
        })
        .catch((error)=>{
            alert("ERROR"+" "+error);
        })
    }
    
}
function borrar()
{
    Estatus=1;
    update(ref(db,'productos/'+IdProducto),{
        Estatus:Estatus,
    }).then(resp=>{
        alert("Se Elimino con Exito.");
        limpiarInputs();
        
    })
    .catch((error)=>{
        alert("ERROR"+" "+error);
    })
}

function existe()
{
    leerInputs();
    if(IdProducto=="" || NombreProducto=="" || Descripcion=="" ||Cantidad==""||Estatus==""||Precio=="")
    {
        alert("NO DEJE CAMPOS VACIOS")
       
    }
    else if(Url=="")
    {
        alert("INSERTE UNA IMAGEN");
        
    }
    else
    {
        const dbref=ref(db);
        get(child(dbref,'productos/'+IdProducto)).then((snapshot)=>{
            if(snapshot.exists())
            {
                alert("YA EXISTE UN PRODUCTO CON ESE ID");
            }
            else{
                insertarDatos();
            }
    
        }).catch((error)=>{
            alert("ERROR"+" "+error);
        });
    }
        
    
    
}
function buscarDatos()
{
    leerInputs();
    if(IdProducto=="")
    {
        alert("NO DEJE CAMPOS VACIOS")
       
    }
    const dbref=ref(db);
    get(child(dbref,'productos/'+IdProducto)).then((snapshot)=>{
        if(snapshot.exists())
        {
            Cantidad=snapshot.val().Cantidad;
            Descripcion=snapshot.val().Descripcion;
            Estatus=snapshot.val().Estatus;
            NombreProducto = snapshot.val().NombreProducto;
            Precio=snapshot.val().Precio;
            Url=snapshot.val().Url;
            
          
            llenarInputs();
            cargarImagen2()
        }
        else
        {
            alert("No existe en la Base de Datos");
        }
    }).catch((error)=>{
        alert("ERROR"+" "+error);
    });

}

function llenarInputs()
{
    document.getElementById('nombre').value=NombreProducto;
    document.getElementById('descripcion').value=Descripcion;
    document.getElementById('cantidad').value=Cantidad;
    document.getElementById('estatus').value=Estatus;
    document.getElementById('precio').value=Precio;
    document.getElementById('url').value=Url;
}

function leerInputs()
{
    IdProducto=document.getElementById('id').value;
    NombreProducto=document.getElementById('nombre').value;
    Descripcion=document.getElementById('descripcion').value;
    Cantidad=document.getElementById('cantidad').value;
    Estatus=document.getElementById('estatus').value;
    Precio=document.getElementById('precio').value;
    Url=document.getElementById('url').value;

}
function camposv(v)
{
    leerInputs();
    if(IdProducto=="" || NombreProducto=="" || Descripcion=="" ||Cantidad==""||Estatus==""||Precio=="")
    {
        alert("NO DEJE CAMPOS EN BLANCO")
        v=0;
    }
    else if(Url=="")
    {
        alert("INSERTE UNA IMAGEN");
        v=0
    }
    else
    {
       return v=1;
    }
}


function limpiarInputs()
{
    document.getElementById('id').value="";
    document.getElementById('nombre').value="";
    document.getElementById('descripcion').value="";
    document.getElementById('cantidad').value="";
   
    document.getElementById('precio').value="";
    document.getElementById('url').value="";
    document.getElementById('imagenp').src="/img/generico.png"

}

async function cargarImagen()
{
    const file=event.target.files[0];
    const name=event.target.files[0].name;

    const storage=getStorage();
    const storageRef = refS(storage, 'imagenes/'+name);
    
    await uploadBytes(storageRef,file).then((snapshot)=>
    {
        descargarImagen(name);
    })
}

async function descargarImagen(name)
{
    const storage =getStorage();
    const starsRef=refS(storage,'imagenes/'+name);


    await getDownloadURL(refS(storage,'imagenes/'+name))
    .then((url)=>{
        document.getElementById('url').value=url;
        cargarImagen2()
    })
}

function cargarImagen2()
{
   var link=document.getElementById('url').value;

    document.getElementById('imagenp').src=link;
}

function mostrarProductos()
{
    const db = getDatabase();
    const dbRef=ref(db, 'productos');
    onValue(dbRef,(snapshot)=>
    {
        lista.innerHTML=""
        snapshot.forEach((childSnapshot)=>{
            const childKey = childSnapshot.key;
            const childData=childSnapshot.val();

            lista.innerHTML="<section class='servicio>"+lista.innerHTML+" "+childKey+" "+childData.NombreProducto+" "+childData.Descripcion+" "+childData.Cantidad+" "+" </section>"
        });

    },{
        onlyOnce:true
    });

}





var btnRegistro=document.getElementById('registrar');
var btnBuscar=document.getElementById('buscar');
var btnModificar=document.getElementById('modificar');
var btnLimpiar=document.getElementById('limpiar');
var archivo=document.getElementById('archivo');
var btnBorrar=document.getElementById('borrar');
btnBorrar.addEventListener('click',borrar);
btnLimpiar.addEventListener('click',limpiarInputs);
btnBuscar.addEventListener('click',buscarDatos);
btnRegistro.addEventListener('click',existe);
btnModificar.addEventListener('click',actualizarDatos);
archivo.addEventListener('change',cargarImagen);